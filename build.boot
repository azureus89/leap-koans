(set-env!
  :source-paths #{"src"}
    :dependencies '[[org.clojure/clojure "1.9.0"]
                    [koan-engine "0.2.5"]
                    [nightlight "RELEASE" :scope "test"]])

(require '[nightlight.boot :refer [nightlight]])

(deftask run []
  (comp
      (wait)
      (nightlight :port 4000)))
