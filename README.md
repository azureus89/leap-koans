# Clojure Koans

The Clojure Koans are a fun way to get started with Clojure - no experience
assumed or required. Follow the instructions below to start making tests pass!


## Getting Started
Navigate to src > koans. Start with 01_equalities, slowly working your way through the different concepts you can master with clojure. Every file is like a level in a game, exposing you to more advanced concepts and getting harder as you go.

Once in the first file, turn on the instaREPL. This will auto evaluate all your code as you type! Start with the first "meditation". The quote above the code is a Zen-esque hint about the solution to line of code that follows immediately after.

If you have successfully turned on the instaREPL - the red block on the left (the REPL evaluation block) indicates which problem you should be solving next. Go through every problem in the file and the REPL evaluation block will turn green. Now you're ready to move to the next file and next set of problems.

Have fun!


## Contributors

https://github.com/functional-koans/clojure-koans/contributors


## Credits

These exercises were started by [Aaron Bedra](http://github.com/abedra) of
[Relevance, Inc.](http://github.com/relevance) in early 2010, as a learning
tool for newcomers to functional programming. Aaron's macro-fu makes these
koans clear and fun to use and improve upon, and without Relevance's
initiative, this project would not exist.

Using the [koans](http://en.wikipedia.org/wiki/koan) metaphor as a tool for
learning a programming language started with the
[Ruby Koans](http://rubykoans.com) by [EdgeCase](http://github.com/edgecase).


## License

The use and distribution terms for this software are covered by the
Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
which can be found in the file epl-v10.html at the root of this distribution.
By using this software in any fashion, you are agreeing to be bound by
the terms of this license.
